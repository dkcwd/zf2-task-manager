<?php

namespace Zf2TaskManagerTest\Listener\Service;

use PHPUnit_Framework_TestCase;

use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Test\Util\ModuleLoader;

class TaskListenerFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function setUp()
    {
        $loader = new ModuleLoader(array(
            'modules' => array(
                'SlmQueue' => new \SlmQueue\Module(),
            ),
            'module_listener_options' => array(
                'config_static_paths' => array(
                    __DIR__ . '/../../../config/app.config.php',
                ),
            ),
        ));

        $this->serviceLocator = $loader->getServiceManager();
        $this->serviceLocator->setAllowOverride(true);
    }

    public function testExecuteStrategyConfiguredResultsInExecuteListenerBuildAttemptViaFactory()
    {
        $config = $this->serviceLocator->get('Config');
        $config['task_manager'] = array(
            'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::EXECUTE
        );
        $this->serviceLocator->setService('Config', $config);

        $listener = $this->serviceLocator->get('Zf2TaskManager\Listener\TaskListener');
        $this->assertInstanceOf('Zf2TaskManager\Listener\Execute\TaskListener', $listener);
    }

    public function testEnqueueStrategyConfiguredResultsInEnqueueListenerBuildAttemptViaFactory()
    {
        $config = $this->serviceLocator->get('Config');
        $config['task_manager'] = array(
            'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::ENQUEUE,
        );
        $this->serviceLocator->setService('Config', $config);

        // This module does not implement any specific vendor queue so expecting an exception here
        $this->setExpectedException('Zend\ServiceManager\Exception\ServiceNotFoundException');

        $listener = $this->serviceLocator->get('Zf2TaskManager\Listener\TaskListener');
        $this->assertInstanceOf('Zf2TaskManager\Listener\Execute\TaskListener', $listener);
    }

}
