<?php

namespace Zf2TaskManagerTest\Event;

use \PHPUnit_Framework_TestCase;

use Zf2TaskManager\Event\TaskEvent;

class TaskEventTest extends PHPUnit_Framework_TestCase
{
    public function testCanBuildEventFromData()
    {
        $taskEvent = new TaskEvent('myEventName', '*', array('data' => 'value'), 'myTaskNameIfDifferent');
        $this->assertEquals('myEventName', $taskEvent->getName());
        $this->assertEquals('*', $taskEvent->getTarget());
        $this->assertEquals(array('data' => 'value'), $taskEvent->getParams());
        $this->assertEquals('myTaskNameIfDifferent', $taskEvent->getTaskName());
    }

}
