<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Zf2TaskManager\Listener\TaskListener' => 'Zf2TaskManager\Listener\Service\TaskListenerFactory',
        ),
    ),
    'task_manager' => array(
        /**
         * Example configuration, use the execute strategy until you are ready to use queueing
         */
        'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::EXECUTE,
//        'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::ENQUEUE,
        'registered_tasks' => array(
//            'send_email' => 'Example\Task\SendEmailTask', //
        )
    ),
);
