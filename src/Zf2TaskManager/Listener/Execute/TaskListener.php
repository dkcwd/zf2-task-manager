<?php

namespace Zf2TaskManager\Listener\Execute;

use Zf2TaskManager\Event\TaskEventInterface;
use Zf2TaskManager\Listener\AbstractTaskListener;

class TaskListener extends AbstractTaskListener
{
    /**
     * @param TaskEventInterface $e
     */
    public function action(TaskEventInterface $e)
    {
        $task = $this->getTask($e);
        $task->execute();
    }

}
