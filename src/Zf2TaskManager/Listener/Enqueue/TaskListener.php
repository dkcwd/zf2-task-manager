<?php

namespace Zf2TaskManager\Listener\Enqueue;

use Zf2TaskManager\Event\TaskEventInterface;
use Zf2TaskManager\Listener\AbstractTaskListener;
use SlmQueue\Queue\QueueAwareTrait;

class TaskListener extends AbstractTaskListener
{
    use QueueAwareTrait;

    /**
     * @param TaskEventInterface $e
     */
    public function action(TaskEventInterface $e)
    {
        $task = $this->getTask($e);
        $queue = $this->getQueue();
        $queue->push($task);
    }

}