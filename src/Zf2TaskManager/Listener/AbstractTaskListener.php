<?php

namespace Zf2TaskManager\Listener;

use Zf2TaskManager\Event\TaskEventInterface;
use SlmQueue\Job\JobInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\EventManager\ListenerAggregateInterface;

abstract class AbstractTaskListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    protected $tasks          = array();
    protected $eventsToAttach = array();

    /**
     * @param array $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @return array
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param array $eventsToAttach
     */
    public function setEventsToAttach($eventsToAttach)
    {
        $this->eventsToAttach = $eventsToAttach;
    }

    /**
     * @return array
     */
    public function getEventsToAttach()
    {
        return $this->eventsToAttach;
    }

    /**
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        foreach ($this->eventsToAttach as $event) {
            if (! $event instanceof TaskEventInterface) continue;

            $events->getSharedManager()->attach(
                $event->getTarget(),
                $event->getName(),
                function ($event) {
                    $this->action($event);
                }
            );
        }
    }

    /**
     * @param  TaskEventInterface $e
     * @return \SlmQueue\Job\JobInterface
     */
    protected function getTask(TaskEventInterface $e)
    {
        $task = $this->getTaskByName($e->getName());
        $task->setContent($e->getParams());

        return $task;
    }

    /**
     * @param  string $taskName
     * @throws \Exception
     * @return \SlmQueue\Job\JobInterface
     */
    protected function getTaskByName($taskName)
    {
        if (! isset($this->tasks[$taskName])) {
            throw new \Exception(
                sprintf(
                    'The task %s could not be resolved so has most likely not been registered correctly',
                    $taskName
                )
            );
        }

        if (! $this->tasks[$taskName] instanceof JobInterface) {
            throw new \Exception('Only tasks which implement \SlmQueue\Job\JobInterface can be requested');
        }

        return $this->tasks[$taskName];
    }

    /**
     * The action which will be to 'queue' or 'execute'
     * @param TaskEventInterface $e
     */
    abstract public function action(TaskEventInterface $e);

}
