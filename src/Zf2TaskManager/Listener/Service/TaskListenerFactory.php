<?php

namespace Zf2TaskManager\Listener\Service;

use Zf2TaskManager\Event\TaskEvent;
use Zf2TaskManager\Listener\Execute\TaskListener as Execute;
use Zf2TaskManager\Listener\Enqueue\TaskListener as Enqueue;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TaskListenerFactory implements FactoryInterface
{
    const ENQUEUE = 'enqueue';
    const EXECUTE = 'execute';

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $registeredTasks = array();
        $strategy = self::EXECUTE;
        $config = $serviceLocator->get('Config');

        if (isset($config['task_manager']) && isset($config['task_manager']['strategy'])) {
            $strategy = $config['task_manager']['strategy'];
        }

        if (isset($config['task_manager']) && isset($config['task_manager']['registered_tasks'])) {
            $registeredTasks = $config['task_manager']['registered_tasks'];
        }

        $listener = null;
        if ($strategy == self::EXECUTE) {
            $listener = new Execute();
        } else {
            /**
             * TODO: implement multiple process queues
             * note: for speed of development/minimal config only 1 queue called 'process queue' is supported
             */
            $listener = new Enqueue();
            $queueManager = $serviceLocator->get('SlmQueue\Queue\QueuePluginManager');
            $listener->setQueue($queueManager->get('processQueue'));
        }

        $tasks = array();
        $eventsToAttach = array();

        /**
         * TODO: benchmark the performance of using the shared event manager with wildcard and potentially refactor
         */
        foreach ($registeredTasks as $friendlyName => $taskName) {
            $eventsToAttach[] = new TaskEvent($friendlyName, '*', array(), $taskName);
            $tasks[$friendlyName] = $serviceLocator->get('SlmQueue\Job\JobPluginManager')->get($taskName);
        }

        $listener->setTasks($tasks);
        $listener->setEventsToAttach($eventsToAttach);

        return $listener;
    }
}
