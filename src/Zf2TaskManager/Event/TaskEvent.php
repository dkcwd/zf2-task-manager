<?php

namespace Zf2TaskManager\Event;

use Zf2TaskManager\Event\TaskEventInterface;
use Zend\EventManager\Event;

class TaskEvent extends Event implements TaskEventInterface
{
    /**
     * @var string Task name
     */
    protected $taskName;

    /**
     * Constructor
     *
     * Accept a target and its parameters.
     *
     * @param  string $name Event name
     * @param  string|object $target
     * @param  array|ArrayAccess $params
     * @param  string $taskName
     */
    public function __construct($name = null, $target = null, $params = null, $taskName = '')
    {
        parent::__construct($name, $target, $params);
        $this->setTaskName((empty($taskName) ? $name : (string) $taskName));
    }

    /**
     * @param string $taskName
     */
    public function setTaskName($taskName)
    {
        $this->taskName = $taskName;
    }

    /**
     * @return string
     */
    public function getTaskName()
    {
        return $this->taskName;
    }

}
