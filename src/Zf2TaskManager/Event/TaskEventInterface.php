<?php

namespace Zf2TaskManager\Event;

interface TaskEventInterface
{
    public function setTaskName($taskName);
    public function getTaskName();
}
