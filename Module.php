<?php

namespace Zf2TaskManager;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'Zf2TaskManager\Listener\TaskListener' => 'Zf2TaskManager\Listener\Service\TaskListenerFactory',
                ),
            ),
        );
    }

    public function onBootstrap(EventInterface $e)
    {
        /** @var \Zend\Mvc\MvcEvent $e */
        $em = $e->getApplication()->getEventManager();
        $sm = $e->getApplication()->getServiceManager();
        $em->attach($sm->get('Zf2TaskManager\Listener\TaskListener'));
    }
}
